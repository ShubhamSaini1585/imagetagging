#!/bin/sh
apk add git
# BRANCH_TAG=$(git ls-remote --tags origin |awk '{print $2}'| awk -F/ '{print $NF}' | grep "1.0.1")

BRANCH_TAG=$(git tag -l)
if [image is being pushed for the first time ]
then
    X=$(echo $BRANCH_TAG | cut -d. -f1)

    Y=$(echo $BRANCH_TAG | cut -d. -f2)

    Z=$(echo $BRANCH_TAG | cut -d. -f3)

    if [[ "$Branch" =~ .*"release".* ]]
    then
        X=$(($X+1))
        echo $X
    fi

    if [[ "$Branch" =~ .*"feature".* ]]
    then
        Y=$(($Y+1))
        echo $Y
    fi

    if [[ "$Branch" =~ .*"Hotfix".* ]]
    then
        Z=$(($Z+1))
        echo $Z
    fi

    TAG_LATEST=$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_NAME:$X.$Y.$Z
    echo $TAG_LATEST
    docker build -t <image_name> .
    docker push TAG_LATEST
fi



if [branch tag already exists]
then
    X=$(echo $BRANCH_TAG | cut -d. -f1)

    Y=$(echo $BRANCH_TAG | cut -d. -f2)

    Z=$(echo $BRANCH_TAG | cut -d. -f3)

    if [[ "$Branch" =~ .*"release".* ]]
    then
        X=$(($X+1))
        echo $X
    fi

    if [[ "$Branch" =~ .*"feature".* ]]
    then
        Y=$(($Y+1))
        echo $Y
    fi

    if [[ "$Branch" =~ .*"Hotfix".* ]]
    then
        Z=$(($Z+1))
        echo $Z
    fi
    TAG_LATEST=$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_NAME:$X.$Y.$Z
    echo $TAG_LATEST
    docker build -t <image_name> .
    docker push $TAG_LATEST
fi