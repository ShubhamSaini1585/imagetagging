#!/bin/bash
BRANCH_TAG=$(git ls-remote --tags origin | sed -r "s/.*\///g" | sort -r)
echo $BRANCH_TAG
regex="[0-9].[0-9].[0-9]"
final_tag=""

for tag in ${BRANCH_TAG[@]}
do
    if [[ $tag =~ $regex ]]
    then
        final_tag=$tag   
        break
    fi
done
echo $final_tag
X=$(echo $final_tag | cut -d. -f1)
Y=$(echo $final_tag | cut -d. -f2)
Z=$(echo $final_tag | cut -d. -f3)
if [[ "$CI_COMMIT_BRANCH" =~ .*"main".* ]]
then
    Z=$(($Z+1))
fi
 
export tagg="$X.$Y.$Z"
echo tag="$tagg" >> build.env
echo $tagg
